# Krita Scripting Pro Website

A small React.js site that is to help document and show examples with doing Krita scripting. While the existing documentation is good for certain high level things, it is more developer focused, and not task focused enough. This site is a bit.

![Krita Scripting site screenshot](/screenshot.png)


## Pre-requisites

NPM - This will allow you to download web dependencies and run it locally


## Building and running locally

After cloning the project, open a terminal/command prompt to the base location that has "package.json".
Type 'npm install'
Run 'npm start'

Locally you might need to upgrade packages for the project to work right. This is usually because of security related concerns on your local computer. Upgrading dependencies can break the production build as it uses a very specic version of NPM. Until we can get more information around which version of NPM to build against, avoid updating any of the packages.json type files.

Note: Locally I am using Node 18.12, npm version 8.19.2. I also had to update node-sass to ^8.0.0 and react-scripts to ^5.0.1. 

A new web browser should open up and it should run. If you want to stop the server, go back to the terminal and type Ctrl + C



## Building and deploying final site

When you push to the master branch on this repository, there will be a build process kicked off. This process will automatically push the changes live. No need for things like FTPing and copying files to update the site.

If you want to see locally how the production build files look, you can go to the root directory (one above src) of the website project. Run this command to do a production build
    
    npm run-script build --prod



## Generating JSON data for website to use

There is a 'generate-actions.py' in the root folder. If you open up Krita and paste this into Scripter, it will generate all the JSON for the actions. This will eventually get copied to src/info-json.js. The JSON in the output isn't assigned to the variable, so just make sure to keep the variable name when pasting it to the info-json file.


## Generating the icon library

There is a 'generate-icons.py' file that has the Python that generates everything. You need the Krita source code to get this. In the Python file, there is a place near the top where you can point to locations. 

You will probably have to create some empty folders for them to go in. The script generates them in this folder.

public/assets/icon-dictionary


When the script is done running it will copy all the icon files over to an icon-dictonary folder in this project. It will also generate a JSON file that has all the metadata the site should need to do its fancy stuff.


## What is the _redirects file
For testing, this site was using Netlify. It is an easy web hosting service where you can just drag your zip file to the web browser and it gives you a URL the site is on. I haven't paid any money for it, but I think if you want a real domain you need to pay them.

The _redirects file tells all server requests to back to the homepage. When you do a build with "npm build -- --prod", copy the _redirects file to that. Then go in the build folder, zip everything up and upload it to Netlify. This will make sure going to URLs directly will work and not give 404s.
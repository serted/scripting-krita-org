import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import './dark-theme.scss'
import App from './App.jsx';

ReactDOM.render(<App />, document.getElementById('root'));

import json
import krita

def getChildrenInfo(target):
    
    dataToExport = []   
   
    for child in target:
        
        # create object we will later turn into JSON
        item = {
              "text": 0,
              "id": "",
              "tooltip": "",
              "defaultShortcut": False,
        }

        # remove & character from object name
        itemText = child.text().replace('&', '')

        item['text'] = itemText        
        item['id'] = child.objectName()
        item['tooltip'] = child.toolTip()
        item['defaultShortcut'] = child.shortcut().toString()    
        
        dataToExport.append(item)
    
    # this puts the final object data in the output area in Scripter
    print(dataToExport)

         
         
getChildrenInfo(Krita.instance().actions())
